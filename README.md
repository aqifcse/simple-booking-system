# simple-booking-system

1. Now clone the project and navigate to simple-booking-system

git clone https://gitlab.com/aqifcse/simple-booking-system.git
cd simple-booking-system

2. create a virtualenvironment

virtualenv venv
source venv/bin/activate

3 . Install all the dependencies for the project.

pip install -r requirements.txt

4 . You are all setup, let’s migrate now.

python manage.py makemigrations
python manage.py migrate

5 . Create a superuser to rule the site 😎

python manage.py createsuperuser

6 . Let’s visit the site now

python manage.py runserver

Visit http://127.0.0.1:8000/